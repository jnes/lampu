from machine import Pin, PWM
import micropython
import random
import rp2
import time
from wavePlayer import wavePlayer
import _thread


# https://docs.micropython.org/en/latest/reference/isr_rules.html
micropython.alloc_emergency_exception_buf(100)

REMOTE_GREEN = 9
REMOTE_RED = 8
REMOTE_KIVI = 7
REMOTE_JINGLE = 6

btn_green = Pin(REMOTE_GREEN, mode=Pin.IN, pull=Pin.PULL_DOWN)
btn_red = Pin(REMOTE_RED, mode=Pin.IN, pull=Pin.PULL_DOWN)
btn_kivi = Pin(REMOTE_KIVI, mode=Pin.IN, pull=Pin.PULL_DOWN)
btn_jingle = Pin(REMOTE_JINGLE, mode=Pin.IN, pull=Pin.PULL_DOWN)

prog_id = None

audio_out = Pin(2, mode=Pin.OUT)
 # dummy assignment for stereo wav lib
audio_nil = Pin(3, mode=Pin.OUT)

blinky = Pin(17, mode=Pin.OUT)

# relay board is inverted
relay_green = Pin(28, mode=Pin.OUT, value=True)
relay_red = Pin(29, mode=Pin.OUT, value=True)

WAV_GREEN = "/audio/green.wav"
WAV_RED = "/audio/red.wav"
WAV_KIVI = "/audio/kivi.wav"
WAV_JINGLE = "/audio/jingle.wav"

player: wavePlayer | None = None

ignore_irq: bool = False

def handle_button(pin: Pin):
    print('handle_button')

    # "Pin(GPIO5, mode=IN, pull=PULL_DOWN)"
    global prog_id

    pid = int(str(pin)[8])
    if pid == prog_id:
        prog_id = None
    else:
        prog_id = pid

    # to break the blocking audio player
    p = player
    if p and hasattr(p, 'dma0'):
        p.stop()

def remote_irq(pin: Pin):
    if ignore_irq:
        print('remote_irq: ignore')
        return

    print('remote_irq: handle')
    micropython.schedule(handle_button, pin)

@rp2.asm_pio(set_init=rp2.PIO.OUT_LOW)
def blink_1decihz():
    # cycles: 1 + 9 + 10 * (28 + 1) = 300
    set(pins, 1)
    set(x, 9).delay(8)
    label("delay_high")
    nop().delay(27)
    jmp(x_dec, "delay_high")

    # cycles: 1 + 9 + 10 * (7 + 31 * (30 + 1) + 1) = 9700
    set(pins, 0)
    set(x, 9).delay(8)
    label("delay_low_x")
    set(y, 30).delay(6)
    label("delay_low")
    nop().delay(29)
    jmp(y_dec, "delay_low")
    jmp(x_dec, "delay_low_x")

def set_lights(green: bool, red: bool):
    """Switching the lights causes an EMP. (Maybe; triggers IRQ multiple times.)"""
    # irq_state = machine.disable_irq() # bricks
    global ignore_irq
    ignore_irq = True
    try:
        relay_green.value(not green)
        relay_red.value(not red)
        time.sleep_ms(10)
    finally:
        # machine.enable_irq(irq_state)
        ignore_irq = False

def do_green():
    global player, prog_id

    set_lights(True, False)

    player = wavePlayer(
        leftPin=audio_out,
        rightPin=audio_nil,
        virtualGndPin=None)

    n = 20
    while prog_id == REMOTE_GREEN and n:
        player.play(WAV_GREEN)
        n -= 1

    if prog_id == REMOTE_GREEN:
        prog_id = None

def do_red():
    global player, prog_id

    set_lights(False, True)

    player = wavePlayer(
        leftPin=audio_out,
        rightPin=audio_nil,
        virtualGndPin=None)

    n = 20
    while prog_id == REMOTE_RED and n:
        player.play(WAV_RED)
        # PWM(audio_out).duty_u16(0)
        # time.sleep_ms(1600)
        n -= 1

    if prog_id == REMOTE_RED:
        prog_id = None

def do_kivi():
    global player, prog_id

    player = wavePlayer(
        leftPin=audio_out,
        rightPin=audio_nil,
        virtualGndPin=None)

    delays = [5]
    for x in range(random.randint(9, 12)):
        delays.append(delays[-1] * (random.randint(15, 18) / 10.))
    delays.append(0)

    while prog_id == REMOTE_KIVI and delays:
        if relay_green.value(): # inverted
            set_lights(True, False)
        else:
            set_lights(False, True)

        player.play(WAV_KIVI)
        PWM(audio_out).duty_u16(0)
        time.sleep_ms(int(delays.pop(0)))

    if prog_id == REMOTE_KIVI:
        if not relay_green.value():
            prog_id = REMOTE_GREEN
        else:
            prog_id = REMOTE_RED

jingle_show = (
    0b01,
    0b11,
    0b10,
    0b00,
    0b01,
    0b11,
    0b10,
    0b00,

    0b10,
    0b11,
    0b01,
    0b00,
    0b10,
    0b11,
    0b01,
    0b00,

    0b11,
    0b10,
    0b01,
    0b00,
    0b11,
    0b01,
    0b10,
    0b00,

    0b11,
    0b01,
    0b10,
    0b00,
    0b11,
    0b10,
    0b01,
    0b00,
)

def jingle_lights():
    time.sleep_ms(100)

    max_disco = len(jingle_show) - 1
    jingle_counter = 0

    while prog_id == REMOTE_JINGLE:
        x = jingle_show[jingle_counter]
        set_lights(x & 0b01, x & 0b10)
        time.sleep_ms(240)
        if jingle_counter == max_disco:
            jingle_counter = 0
        else:
            jingle_counter += 1

def do_jingle():
    global player, prog_id

    player = wavePlayer(
        leftPin=audio_out,
        rightPin=audio_nil,
        virtualGndPin=None)

    _thread.start_new_thread(jingle_lights, ())

    player.play(WAV_JINGLE)

    if prog_id == REMOTE_JINGLE:
        prog_id = None

def init():
    for btn in (btn_green, btn_red, btn_kivi, btn_jingle):
        btn.irq(handler=remote_irq,
                trigger=Pin.IRQ_RISING,
                hard=True, # unreliable
                )

    sm = rp2.StateMachine(0, blink_1decihz, freq=2000, set_base=blinky)
    sm.active(True)

init()

def run_loop():
    global player, prog_id
    while True:
        set_lights(False, False)

        # may not be initialized (used) yet
        p, player = player, None
        if p and hasattr(p, 'dma0'):
            p.stop()

        # may be putting out some garbage audio, stop it
        PWM(audio_out).duty_u16(0)

        if prog_id == REMOTE_GREEN:
            print('do_green()')
            do_green()
        elif prog_id == REMOTE_RED:
            print('do_red()')
            do_red()
        elif prog_id == REMOTE_KIVI:
            print('do_kivi()')
            do_kivi()
        elif prog_id == REMOTE_JINGLE:
            print('do_jingle()')
            do_jingle()
        else:
            print('sleep')
            time.sleep_ms(100)

run_loop()
